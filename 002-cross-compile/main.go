package main

import "fmt"

import "runtime"

func main() {
	fmt.Printf("Sistema operativo %v\nArquitectura: %v\n", runtime.GOOS, runtime.GOARCH)
}
