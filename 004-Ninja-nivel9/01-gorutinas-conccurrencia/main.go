package main

import(

	"fmt"
	"runtime"
	"sync"

)

func main() {

	fmt.Printf("Numero de CPU inicio: %v\n", runtime.NumCPU())

	fmt.Printf("Numero de GOrutinas inicio: %v\n", runtime.NumGoroutine())

	var wg sync.WaitGroup

	wg.Add(2)



	go func(){
	
		fmt.Println("Hola desde la primera Gorutina")

		wg.Done()
	
	}()

	go func(){

		fmt.Println("Hola desde la segunda Gorutina")

		wg.Done()

	}()

	fmt.Printf("Numero de CPU Medio: %v\n", runtime.NumCPU())

	fmt.Printf("Numero de GOrutinas Medio: %v\n", runtime.NumGoroutine())

	wg.Wait()

	fmt.Println("A punto de finalizar main")

	fmt.Printf("Numero de CPU Final: %v\n", runtime.NumCPU())

	fmt.Printf("Numero de GOrutinas Final: %v\n", runtime.NumGoroutine())

}
